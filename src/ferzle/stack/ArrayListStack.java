package ferzle.stack;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A simple stack class based on an ArrayList
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class ArrayListStack<T> implements StackInterface<T> {
   ArrayList<T> theElements;
   
    public ArrayListStack() {
        theElements = new ArrayList<T>();
    }
    
    public boolean isEmpty() {
        return theElements.size()==0;
    }
    
    public boolean isFull() {
        return false;
    }
    
    public boolean push(T item) {
        theElements.add(item);   
        return true;
    }
    public T pop() {
        if(isEmpty()) {
            return null;
        } else {
            return theElements.remove(theElements.size()-1);
        }
    } 
    
    public T peek() {
        if(isEmpty()) {
            return null;
        } else {
            return theElements.get(theElements.size()-1);
        }
    }
    public Iterator<T> iterator() {
        // As we will see when we run the tests, this isn't actually correct.
    	// How could we fix it?
        return theElements.iterator();
    }
}
